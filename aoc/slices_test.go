package aoc

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestSliceSort(t *testing.T) {
	input := []string{"abc", "xyz", "def"}
	sorted := Sort(input)
	require.Equal(t, []string{"abc", "def", "xyz"}, sorted)

	inputN := []int{1, 4, 3, 2}
	sortedN := Sort(inputN)
	require.Equal(t, []int{1, 2, 3, 4}, sortedN)

	inputF := []float64{1.4, 4.1, 3.2, 2.3}
	sortedF := Sort(inputF)
	require.Equal(t, []float64{1.4, 2.3, 3.2, 4.1}, sortedF)
}
