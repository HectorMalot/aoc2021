package aoc

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestLinkedList(t *testing.T) {
	elems := []string{"ab", "cd", "ef"}
	ll := NewLinkedList(elems)

	require.Equal(t, 3, ll.Len())
	require.True(t, ll.Step(1))
	ll.Insert("de")
	require.Equal(t, 4, ll.Len())
	require.True(t, ll.Step(1))
	ll.InsertBehind(("fg"))
	ll.StepBack(1)
	require.Equal(t, "fg", ll.Current.Value)
	ll.Step(1)
	require.False(t, ll.Step(1))
}
