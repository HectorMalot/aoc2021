package aoc

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestNumbers(t *testing.T) {
	t.Run("max", func(t *testing.T) {
		require.Equal(t, 6, Max([]int{1, 2, 3, 4, 5, 6, 2, 3, 4}))
	})

	t.Run("GCD", func(t *testing.T) {
		require.Equal(t, 4, GCD(8, 12))
	})

	t.Run("LCM", func(t *testing.T) {
		require.Equal(t, 24, LCM(3, 4, 8))
	})
}

func TestWindow(t *testing.T) {
	input := []int{1, 2, 3, 4, 5}
	windowed := Window(input, 3, func(t []int) int { return Sum(t) })
	require.Equal(t, []int{6, 9, 12}, windowed)
}
