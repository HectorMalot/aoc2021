package aoc

type Set[T comparable] struct {
	m map[T]struct{}
}

func NewSet[T comparable](ss ...T) *Set[T] {
	m := make(map[T]struct{}, len(ss))
	for _, s := range ss {
		m[s] = struct{}{}
	}
	res := Set[T]{m: m}
	return &res
}

func (set *Set[T]) Add(s T) bool {
	added := !set.Contains(s)
	set.m[s] = struct{}{}
	return added
}

func (set *Set[T]) Delete(s T) bool {
	deleted := set.Contains(s)
	delete(set.m, s)
	return deleted
}

func (set *Set[T]) Contains(s T) bool {
	_, ok := set.m[s]
	return ok
}

func (set *Set[T]) Len() int {
	return len(set.m)
}

func (set *Set[T]) Elems() []T {
	ss := make([]T, 0, len(set.m))
	for s := range set.m {
		ss = append(ss, s)
	}
	return ss
}

func (set *Set[T]) Union(other *Set[T]) *Set[T] {
	u := NewSet[T]()
	for s := range set.m {
		u.m[s] = struct{}{}
	}
	for s := range other.m {
		u.m[s] = struct{}{}
	}
	return u
}

func (set *Set[T]) Difference(other *Set[T]) *Set[T] {
	u := NewSet[T]()
	for s := range set.m {
		if !other.Contains(s) {
			u.Add(s)
		}
	}
	return u
}

func (set *Set[T]) Intersection(other *Set[T]) *Set[T] {
	u := NewSet[T]()
	for s := range set.m {
		if other.Contains(s) {
			u.Add(s)
		}
	}
	return u
}

func (set *Set[T]) Equal(other *Set[T]) bool {
	if set.Len() != other.Len() {
		return false
	}
	for s := range set.m {
		if !other.Contains(s) {
			return false
		}
	}
	return true
}
