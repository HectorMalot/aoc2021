package aoc

type Pos3D struct {
	X, Y, Z int
}

func (p Pos3D) ManhattanDist(q Pos3D) int {
	return Abs(p.X-q.X) + Abs(p.Y-q.Y) + Abs(p.Z-q.Z)
}

func (p Pos3D) Add(x, y, z int) Pos3D {
	return Pos3D{p.X + x, p.Y + y, p.Z + z}
}

func (p Pos3D) Rel(q Pos3D) Pos3D {
	return Pos3D{p.X - q.X, p.Y - q.Y, p.Z - q.Z}
}

func (p Pos3D) Equal(q Pos3D) bool {
	return p.X == q.X && p.Y == q.Y && p.Z == q.Z
}

// Rotates in 90 degree increments, n times per axis, x then y then z
func (p Pos3D) Rotate(x, y, z int) Pos3D {
	for i := 0; i < x; i++ {
		p.Y, p.Z = -p.Z, p.Y
	}
	for i := 0; i < y; i++ {
		p.X, p.Z = -p.Z, p.X
	}
	for i := 0; i < z; i++ {
		p.X, p.Y = p.Y, -p.X
	}
	return p
}

/*
Rotate around X => X values stay the same
y becomes z
z becomes -y


*/
