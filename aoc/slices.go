package aoc

import (
	"constraints"
	"sort"
)

// MinimumIndex returns the index of the element that produces the smallest value
// of fn(Element).
func MinimumIndex[T constraints.Integer, E any](elems []E, fn func(E) T) T {
	var i, f, min, mini T
	if len(elems) == 0 {
		return 0
	}

	min = fn(elems[0])
	for i = 1; i < T(len(elems)); i++ {
		if f = fn(elems[i]); f < min {
			min, mini = f, i
		}
	}
	return mini
}

// Sort sorts the provided slice in ascending order
func Sort[E constraints.Ordered](elems []E) []E {
	sort.Slice(elems, func(i, j int) bool { return elems[i] < elems[j] })
	return elems
}

func Map[T1 any, T2 any](list []T1, fn func(T1) T2) []T2 {
	res := make([]T2, 0, len(list))
	for _, e := range list {
		res = append(res, fn(e))
	}
	return res
}

func Inject[T1, T2 any](list []T1, acc T2, fn func(T1, T2) T2) T2 {
	for _, e := range list {
		acc = fn(e, acc)
	}
	return acc
}

func Filter[E any](elems []E, fn func(E) bool) []E {
	var res []E
	for _, e := range elems {
		if fn(e) {
			res = append(res, e)
		}
	}
	return res
}

func Contains[E comparable](elems []E, e E) bool {
	for _, el := range elems {
		if el == e {
			return true
		}
	}
	return false
}
