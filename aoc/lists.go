package aoc

type LinkedList[T any] struct {
	Head, Tail *node[T]
	Current    *node[T]
	length     int
}

type node[T any] struct {
	Prev  *node[T]
	Next  *node[T]
	Value T
}

func NewLinkedList[T any](elems []T) *LinkedList[T] {
	ll := &LinkedList[T]{}
	for _, e := range elems {
		ll.Push(e)
	}
	return ll
}

// Inserts and element at the end of the list
func (ll *LinkedList[T]) Push(v T) {
	if ll.Tail == nil {
		ll.Head = &node[T]{Value: v}
		ll.Tail = ll.Head
		ll.Current = ll.Head
		ll.length++
	} else {
		ll.Tail.Next = &node[T]{Value: v, Prev: ll.Tail}
		ll.Tail = ll.Tail.Next
		ll.length++
	}
}

// Inserts an element after the 'current' position
// after the insertion, the current pointer will be on the
// new element
func (ll *LinkedList[T]) Insert(v T) {
	if ll.Current == nil { // current not set
		ll.Current = ll.Head
	}
	if ll.Tail == nil || ll.Current.Next == nil { // empty LL or at last node
		ll.Push(v)
	}
	ll.Current.Next.Prev = &node[T]{Value: v, Next: ll.Current.Next, Prev: ll.Current}
	ll.Current.Next = ll.Current.Next.Prev
	ll.Current = ll.Current.Next
	ll.length++
}

// Inserts an element before the 'current' position
// after the insertion, the current pointer will be on the
// current element
func (ll *LinkedList[T]) InsertBehind(v T) {
	if ll.Current == nil { // current not set
		ll.Current = ll.Head
	}
	if ll.Current.Prev == nil { // at first node
		ll.Current.Prev = &node[T]{Value: v, Next: ll.Current, Prev: nil}
	}
	ll.Current.Prev.Next = &node[T]{Value: v, Next: ll.Current, Prev: ll.Current.Prev}
	ll.Current.Prev = ll.Current.Prev.Next
	ll.length++
}

func (ll *LinkedList[T]) Len() int {
	return ll.length
}

// Resets the current pointer to the head of the list
func (ll *LinkedList[T]) Reset() {
	ll.Current = ll.Head
}

func (ll *LinkedList[T]) Step(n int) bool {
	for i := 0; i < n; i++ {
		if ll.Current.Next == nil {
			return false
		}
		ll.Current = ll.Current.Next
	}
	return true
}

func (ll *LinkedList[T]) StepBack(n int) bool {
	for i := 0; i < n; i++ {
		if ll.Current.Prev == nil {
			return false
		}
		ll.Current = ll.Current.Prev
	}
	return true
}

func (ll *LinkedList[T]) Inject(fn func(e T, acc T) T) T {
	ll.Current = ll.Head
	acc := ll.Current.Value
	for ll.Step(1) {
		acc = fn(ll.Current.Value, acc)
	}
	return acc
}
