package aoc

func Keys[T1 comparable, T2 any](m map[T1]T2) []T1 {
	var res []T1
	for k := range m {
		res = append(res, k)
	}
	return res
}

func Vals[T1 comparable, T2 any](m map[T1]T2) []T2 {
	var res []T2
	for _, v := range m {
		res = append(res, v)
	}
	return res
}
