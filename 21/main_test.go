package main

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/hectormalot/aoc2021/aoc"
)

func TestDie(t *testing.T) {
	d := die{0}
	require.Equal(t, 1, d.nextVal())
	require.Equal(t, 2, d.nextVal())
	require.Equal(t, 3, d.nextVal())

	d = die{98}
	require.Equal(t, 99, d.nextVal())
	require.Equal(t, 100, d.nextVal())
	require.Equal(t, 1, d.nextVal())

	d = die{0}
	require.Equal(t, []int{1, 2, 3, 4}, d.roll(4))

}

func TestGame(t *testing.T) {
	g := game{
		p1pos: 3, p2pos: 7,
		turn: 1,
		die:  die{0},
	}
	for aoc.Max([]int{g.p1score, g.p2score}) < 1000 {
		g.iter()
	}
	require.Equal(t, 9, g.p1pos)
	require.Equal(t, 2, g.p2pos)
	require.Equal(t, 1000, g.p1score)
	require.Equal(t, 745, g.p2score)
	require.Equal(t, 993, g.rolls)
}

func TestDirac(t *testing.T) {
	// is := make([]int, 0)
	// for i := 1; i < 4; i++ {
	// 	for j := 1; j < 4; j++ {
	// 		for n := 1; n < 4; n++ {
	// 			fmt.Printf("%d + %d + %d = %d\n", i, j, n, i+j+n)
	// 			is = append(is, i+j+n)
	// 		}
	// 	}
	// }
	// fmt.Println(aoc.Sort(is))
	// g := game{
	// 	p1pos: 7, p2pos: 8,
	// 	turn:   1,
	// 	weight: 1,
	// }
	g := game{
		p1pos: 3, p2pos: 7, turn: 1,
	}
	p1, p2 := g.dirac()
	require.Equal(t, 444356092776315, p1)
	require.Equal(t, 341960390180808, p2)
}
