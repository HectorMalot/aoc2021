package main

import (
	"fmt"
	"time"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

var (
	state map[string]result
)

type result struct {
	p1, p2 int
}

func main() {
	tStart := time.Now()
	// Player 1 starting position: 8 (7 because we are 0 indexed)
	// Player 2 starting position: 9 (8 because we are 0 indexed)
	g := game{p1pos: 7, p2pos: 8, turn: 1, die: die{0}}
	for aoc.Max([]int{g.p1score, g.p2score}) < 1000 {
		g.iter()
	}
	fmt.Println("Problem 1:", aoc.Min([]int{g.p1score, g.p2score})*g.rolls) // 512442
	fmt.Println("Solver took:", time.Since(tStart))

	tStart = time.Now()
	state = make(map[string]result)
	g = game{p1pos: 7, p2pos: 8, turn: 1}
	p1, p2 := g.dirac()
	fmt.Println("Problem 2 (memoized, objects):", aoc.Max([]int{p1, p2})) // 346642902541848
	fmt.Println("Solver took:", time.Since(tStart))

	tStart = time.Now()
	state = make(map[string]result)
	p1, p2 = dirac(7, 0, 8, 0, 1, 0)
	fmt.Println("Problem 2 (memoized, ints):", aoc.Max([]int{p1, p2})) // 346642902541848
	fmt.Println("Solver took:", time.Since(tStart))
}

type game struct {
	p1pos   int
	p2pos   int
	p1score int
	p2score int
	turn    int
	die     die
	rolls   int
}

func (g *game) iter() {
	g.rolls += 3
	steps := aoc.Sum(g.die.roll(3))
	g.move(steps)

}

func (g *game) move(steps int) *game {
	if g.turn == 1 {
		g.p1pos = (g.p1pos + steps) % 10
		g.p1score += g.p1pos + 1
		g.turn = 2
	} else {
		g.p2pos = (g.p2pos + steps) % 10
		g.p2score += g.p2pos + 1
		g.turn = 1
	}
	return g
}

type die struct {
	val int
}

func (d *die) roll(n int) []int {
	res := make([]int, n)
	for i := 0; i < n; i++ {
		res[i] = d.nextVal()
	}
	return res
}

func (d *die) nextVal() int {
	d.val = d.val + 1
	if d.val > 100 {
		d.val = 1
	}
	return d.val
}

/*
Problem 2:

3 throws with dirac dice results in 3*3*3 = 27 universes
but many of these are identical. There are 7 unique universes
3 4 4 4 5 5 5 5 5 5 6 6 6 6 6 6 6 7 7 7 7 7 7 8 8 8 9
*/
func (g *game) dirac() (int, int) {
	if g.p1score > 20 {
		return 1, 0
	}
	if g.p2score > 20 {
		return 0, 1
	}

	// Skip if we've seen this state before
	if r, ok := state[g.serial()]; ok {
		return r.p1, r.p2
	}

	// roll the dice => i.e. run this for all 7 options
	p1wins := make([]int, 7)
	p2wins := make([]int, 7)
	// 1x 1 1 1 = 3
	p1wins[0], p2wins[0] = g.dup().move(3).dirac()
	// 3x 4
	p1wins[1], p2wins[1] = g.dup().move(4).dirac()
	// 6x 5
	p1wins[2], p2wins[2] = g.dup().move(5).dirac()
	// 7x 6
	p1wins[3], p2wins[3] = g.dup().move(6).dirac()
	// 6x 7
	p1wins[4], p2wins[4] = g.dup().move(7).dirac()
	// 3x 8
	p1wins[5], p2wins[5] = g.dup().move(8).dirac()
	// 1x 9
	p1wins[6], p2wins[6] = g.dup().move(9).dirac()

	p1w := aoc.Sum([]int{
		p1wins[0] * 1,
		p1wins[1] * 3,
		p1wins[2] * 6,
		p1wins[3] * 7,
		p1wins[4] * 6,
		p1wins[5] * 3,
		p1wins[6] * 1,
	})
	p2w := aoc.Sum([]int{
		p2wins[0] * 1,
		p2wins[1] * 3,
		p2wins[2] * 6,
		p2wins[3] * 7,
		p2wins[4] * 6,
		p2wins[5] * 3,
		p2wins[6] * 1,
	})
	state[g.serial()] = result{p1w, p2w}
	return p1w, p2w
}

func (g *game) dup() *game {
	g2 := game{
		p1score: g.p1score,
		p2score: g.p2score,
		p1pos:   g.p1pos,
		p2pos:   g.p2pos,
		turn:    g.turn,
	}
	return &g2
}

func (g *game) serial() string {
	return fmt.Sprintf("%d-%d-%d-%d-%d", g.p1pos, g.p1score, g.p2pos, g.p2score, g.turn)
}

func dirac(p1pos, p1score, p2pos, p2score, turn, steps int) (int, int) {
	if steps != 0 {
		if turn == 1 {
			p1pos = (p1pos + steps) % 10
			p1score += p1pos + 1
			turn = 2
		} else {
			p2pos = (p2pos + steps) % 10
			p2score += p2pos + 1
			turn = 1
		}
	}
	if p1score > 20 {
		return 1, 0
	}
	if p2score > 20 {
		return 0, 1
	}

	// Skip if we've seen this state before
	if r, ok := state[serial(p1pos, p1score, p2pos, p2score, turn)]; ok {
		return r.p1, r.p2
	}

	// roll the dice => i.e. run this for all 7 options
	p1wins := make([]int, 7)
	p2wins := make([]int, 7)
	// 1x 1 1 1 = 3
	p1wins[0], p2wins[0] = dirac(p1pos, p1score, p2pos, p2score, turn, 3)
	// 3x 4
	p1wins[1], p2wins[1] = dirac(p1pos, p1score, p2pos, p2score, turn, 4)
	// 6x 5
	p1wins[2], p2wins[2] = dirac(p1pos, p1score, p2pos, p2score, turn, 5)
	// 7x 6
	p1wins[3], p2wins[3] = dirac(p1pos, p1score, p2pos, p2score, turn, 6)
	// 6x 7
	p1wins[4], p2wins[4] = dirac(p1pos, p1score, p2pos, p2score, turn, 7)
	// 3x 8
	p1wins[5], p2wins[5] = dirac(p1pos, p1score, p2pos, p2score, turn, 8)
	// 1x 9
	p1wins[6], p2wins[6] = dirac(p1pos, p1score, p2pos, p2score, turn, 9)

	p1w := aoc.Sum([]int{
		p1wins[0] * 1,
		p1wins[1] * 3,
		p1wins[2] * 6,
		p1wins[3] * 7,
		p1wins[4] * 6,
		p1wins[5] * 3,
		p1wins[6] * 1,
	})
	p2w := aoc.Sum([]int{
		p2wins[0] * 1,
		p2wins[1] * 3,
		p2wins[2] * 6,
		p2wins[3] * 7,
		p2wins[4] * 6,
		p2wins[5] * 3,
		p2wins[6] * 1,
	})
	state[serial(p1pos, p1score, p2pos, p2score, turn)] = result{p1w, p2w}
	return p1w, p2w
}

func serial(p1pos, p1score, p2pos, p2score, turn int) string {
	return fmt.Sprintf("%d-%d-%d-%d-%d", p1pos, p1score, p2pos, p2score, turn)
}
