package main

import (
	"fmt"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

type move struct {
	direction string
	amount    int
}

func main() {
	in := aoc.Lines(aoc.Input(2021, 2))
	var moves []move
	for _, l := range in {
		var m move
		fmt.Sscanf(l, "%s %d", &m.direction, &m.amount)
		moves = append(moves, m)
	}

	// 1
	pos := aoc.Pos{X: 0, Y: 0}
	for _, m := range moves {
		switch m.direction {
		case "up":
			pos = pos.Add(0, -1*m.amount)
		case "down":
			pos = pos.Add(0, m.amount)
		case "forward":
			pos = pos.Add(m.amount, 0)
		}
	}
	fmt.Println("Problem 1:", pos)
	fmt.Println(pos.X * pos.Y)

	// 2
	pos = aoc.Pos{X: 0, Y: 0}
	aim := 0
	for _, m := range moves {
		switch m.direction {
		case "up":
			aim -= m.amount
		case "down":
			aim += m.amount
		case "forward":
			pos = pos.Add(m.amount, aim*m.amount)
		}
	}
	fmt.Println("Problem 2:", pos)
	fmt.Println(pos.X * pos.Y)

}
