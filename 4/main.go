package main

import (
	"fmt"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

type BingoNum struct {
	val    int
	marked bool
}

func main() {
	lines := aoc.Lines(aoc.Input(2021, 4))
	sequence := aoc.IntList(lines[0])
	fmt.Println(sequence)

	// Generate the boards
	boards := make([]*aoc.Board[BingoNum], 0)
	var board *aoc.Board[BingoNum]
	idx := 0
	for _, line := range lines[1:] {
		if line == "" { // Setup new board
			board = aoc.NewBoard[BingoNum](5, 5)
			boards = append(boards, board)
			idx = 0
			continue
		}
		board.SetRow(idx, BingoNumsFromIntlist(aoc.IntList(line)))
		idx++
	}

	// apply the bingo sequence
	b, winningNum := PlayBingo(sequence, boards)
	elems := b.Elements()
	unmarked := aoc.Sum(aoc.Map(aoc.Filter(elems, func(e *BingoNum) bool { return !e.marked }), func(e *BingoNum) int { return e.val }))

	// Problem 1:
	fmt.Println(winningNum * unmarked)

	// Problem 2: last board to win - keep playing until we reach board len(boards) -1
	iterations := len(boards) - 1 // 1 already done
	won := aoc.NewBoard[BingoNum](1, 1)
	won.SetRow(0, []*BingoNum{{-1, false}})
	for i := 0; i < iterations; i++ {
		*b = *won // replace won boards with boards that can'b be completed (-1 values)
		b, winningNum = PlayBingo(sequence, boards)
	}
	elems = b.Elements()
	unmarked = aoc.Sum(aoc.Map(aoc.Filter(elems, func(e *BingoNum) bool { return !e.marked }), func(e *BingoNum) int { return e.val }))
	fmt.Println(winningNum * unmarked)
}

func PlayBingo(sequence []int, boards []*aoc.Board[BingoNum]) (*aoc.Board[BingoNum], int) {
	for _, num := range sequence {
		// mark bingo numbers and check validity for each board (assuming first board to call wins)
		for _, b := range boards {
			b.Apply(func(e *BingoNum) {
				if e.val == num {
					e.marked = true
				}
			})
			// check if we have a bingo
			x, y := b.Size()
			for i := 0; i < y; i++ {
				if b.AllValidRow(i, func(b *BingoNum) bool { return b.marked }) {
					return b, num
				}
			}
			for i := 0; i < x; i++ {
				if b.AllValidCol(i, func(b *BingoNum) bool { return b.marked }) {
					return b, num
				}
			}
		}
	}
	panic("No Bingo")
}

func BingoNumsFromIntlist(list []int) []*BingoNum {
	var res []*BingoNum
	for _, n := range list {
		res = append(res, NewBingoNum(n))
	}
	return res
}

func NewBingoNum(n int) *BingoNum {
	return &BingoNum{val: n, marked: false}
}

func (b *BingoNum) String() string {
	if b.marked {
		return fmt.Sprintf("%3d* ", b.val)
	}
	return fmt.Sprintf("%3d  ", b.val)
}
