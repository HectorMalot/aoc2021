package main

import (
	"fmt"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

type track struct {
	last  int
	count int
}

func main() {
	ints := aoc.StringToIntList(aoc.Lines(aoc.Input(2021, 1)))
	countIncFn := func(v int, tr track) track {
		if v > tr.last {
			tr.count++
		}
		tr.last = v
		return tr
	}

	// Problem 1
	res := aoc.Inject(ints, track{ints[0], 0}, countIncFn)
	fmt.Println("increases for window of 1:", res.count) // 1548

	// Problem 2
	res = aoc.Inject(aoc.Window(ints, 3, func(t []int) int { return aoc.Sum(t) }), track{aoc.Sum(ints[0:3]), 0}, countIncFn)
	fmt.Println("increases for window of 3:", res.count) // 1589
}
