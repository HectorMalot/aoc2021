package main

import (
	"fmt"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

type Line struct {
	from aoc.Pos
	to   aoc.Pos
}

func main() {
	lines := aoc.Lines(aoc.Input(2021, 5))

	// Parse input
	var ventlines []Line
	var maxX, maxY int
	for _, line := range lines { // 692,826 -> 692,915
		l := Line{}
		fmt.Sscanf(line, "%d,%d -> %d,%d", &l.from.X, &l.from.Y, &l.to.X, &l.to.Y)
		ventlines = append(ventlines, l)
		maxX = aoc.Max([]int{l.from.X, l.to.X, maxX})
		maxY = aoc.Max([]int{l.from.Y, l.to.Y, maxY})
	}

	// Problem 1
	b := aoc.NewIntBoard(maxX+1, maxY+1)
	for _, l := range ventlines {
		if !l.Straight() {
			continue
		}
		currentPos := l.from
		for {
			b.Add(currentPos.X, currentPos.Y, 1)
			if currentPos.Equal(l.to) {
				break
			}
			currentPos = currentPos.StrideTowards(l.to)
		}
	}

	count := b.Inject(0, func(acc, e int) int {
		if e > 1 {
			return acc + 1
		}
		return acc
	})

	fmt.Println(count) // 5698

	// Add diagonals
	for _, l := range ventlines {
		if l.Straight() {
			continue
		}
		currentPos := l.from
		for {
			b.Add(currentPos.X, currentPos.Y, 1)
			if currentPos.Equal(l.to) {
				break
			}
			currentPos = currentPos.StrideTowards(l.to)
		}
	}

	count = b.Inject(0, func(acc, e int) int {
		if e > 1 {
			return acc + 1
		}
		return acc
	})

	fmt.Println(count) // 15463
}

func (l Line) Straight() bool {
	if l.from.X == l.to.X || l.from.Y == l.to.Y {
		return true
	}
	return false
}
