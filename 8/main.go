package main

import (
	"fmt"
	"strings"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

func main() {
	lines := aoc.Lines(aoc.Input(2021, 8))

	// Problem 1 // count number of 1 4 7 and 8ths
	sum := 0
	outcomes := make([][]string, len(lines))
	for i, l := range lines {
		rhs := strings.Split(l, " | ")[1]
		outcomes[i] = strings.Fields(rhs)
		for _, o := range outcomes[i] {
			if len(o) == 2 || len(o) == 4 || len(o) == 3 || len(o) == 7 {
				sum++
			}
		}
	}
	fmt.Println(sum)

	// Problem 2
	answers := []int{}
	for _, line := range lines {
		p := newPuzzle(line)
		p.Solve()
		answers = append(answers, p.value())
	}
	fmt.Println("Problem 2: ", aoc.Sum(answers))
}

type puzzle struct {
	in       []*aoc.Set[string]
	out      []*aoc.Set[string]
	solution map[string]int
	cache    map[int]*aoc.Set[string]
}

func newPuzzle(line string) *puzzle {
	split1 := strings.Split(line, " | ")
	// inputs
	measures := strings.Fields(split1[0])
	in := []*aoc.Set[string]{}
	for _, m := range measures {
		in = append(in, aoc.NewSet(aoc.Characters(m)...))
	}

	// outputs
	results := strings.Fields(split1[1])
	out := []*aoc.Set[string]{}
	for _, res := range results {
		out = append(out, aoc.NewSet(aoc.Characters(res)...))
	}

	return &puzzle{
		in:       in,
		out:      out,
		solution: make(map[string]int),
		cache:    make(map[int]*aoc.Set[string]),
	}
}

func (p *puzzle) Solve() {
	// First the simple ones
	for _, m := range p.in {
		switch m.Len() {
		case 2:
			p.solution[setToString(m)] = 1
			p.cache[1] = m
		case 4:
			p.solution[setToString(m)] = 4
			p.cache[4] = m
		case 3:
			p.solution[setToString(m)] = 7
			p.cache[3] = m
		case 7:
			p.solution[setToString(m)] = 8
			p.cache[7] = m
		default:
			continue
		}
	}

	// okay, round 2 for 6, 3 and (9|0)
	for _, m := range p.in {
		switch m.Len() {
		case 5:
			// Can only solve for 3 by knowing it has an intersect of 2 with 1
			one := p.cache[1]
			if one.Intersection(m).Len() == 2 {
				// This is a 3
				p.solution[setToString(m)] = 3
				p.cache[3] = m
			}
			// this is a 2 or 5, to be solved later

		case 6:
			// solving for 6 and ( 9 or 0 )
			one := p.cache[1]
			if one.Intersection(m).Len() == 1 {
				// this is a 6
				p.solution[setToString(m)] = 6
				p.cache[6] = m

			} else {
				// this is a 9 or 0
				// settong to 9, fixing zeros in next iter
				p.solution[setToString(m)] = 9
				p.cache[9] = m
			}
		default:
			continue
		}
	}

	// Round 3 for 2 and 5's and 0
	for _, m := range p.in {
		switch m.Len() {
		case 6:
			// spot zeroes
			one := p.cache[1]
			three := p.cache[3]
			if three.Intersection(m).Len() == 4 && one.Intersection(m).Len() != 1 {
				// zero
				p.solution[setToString(m)] = 0
				p.cache[0] = m

			}
		case 5:
			one := p.cache[1]
			six := p.cache[6]
			switch {
			case one.Intersection(m).Len() == 2:
				// this is a 3, already handled
				continue
			case six.Intersection(m).Len() == 4:
				// this is a 2
				p.solution[setToString(m)] = 2
				p.cache[2] = m
			case six.Intersection(m).Len() == 5:
				// this is a 5
				p.solution[setToString(m)] = 5
				p.cache[5] = m
			default:
				continue
			}
		default:
			continue
		}
	}

}

func (p *puzzle) value() int {
	digits := make([]int, 4)
	for i, v := range p.out {
		k := setToString(v)
		digits[i] = p.solution[k]
	}
	return aoc.Atoi(fmt.Sprintf("%d%d%d%d", digits[0], digits[1], digits[2], digits[3]))
}

func setToString(s *aoc.Set[string]) string {
	elems := s.Elems()
	return strings.Join(aoc.Sort(elems), "")
}
