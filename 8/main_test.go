package main

import (
	"fmt"
	"testing"
)

func TestSolver(t *testing.T) {
	input := "acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf"
	p := newPuzzle(input)
	p.Solve()

	fmt.Println(p.solution)
	fmt.Println(p.value())
	t.FailNow()
}
