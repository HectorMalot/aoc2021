package main

import (
	"fmt"
	"sort"
	"time"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

func main() {
	start := time.Now()

	// Problem 1
	// Dimi: matrix = parse(inp)
	matrix := parse(2021, 9)
	// Dimi: mins, _ = find_minimums(matrix)
	mins, _ := findMinimums(matrix)
	// Dimi: minsum = sum(map(lambda x: x + 1, mins))
	minsum := aoc.Sum(aoc.Map(mins, func(v int) int { return v + 1 }))
	// Dimi: print(f"p1: {minsum}")
	fmt.Println("p1: ", minsum)

	// Dimi: topk_basins = find_topk_basins(matrix, 3)
	topKBasins := findTopKBasins(matrix, 3)
	// Dimi: prod = reduce(lambda x, y: x * y, [len(el) for el in topk_basins])
	prod := aoc.Inject(topKBasins, 1, func(b *aoc.Set[aoc.Pos], acc int) int { return acc * b.Len() })
	// Dimi: end = time()
	// Dimi: print(f"p2: {prod}")
	fmt.Println("p2: ", prod)
	// Dimi: print(end - start)
	fmt.Println(time.Since(start))

}

// def parse(filename: str) -> List[List[int]]:
func parse(year, day int) [][]int {
	raw := aoc.Lines(aoc.Input(year, day))
	var res [][]int
	for _, line := range raw {
		res = append(res, aoc.Digits(line))
	}
	return res
}

// def get_neighbors(matrix: List[List[int]],i: int,j: int) -> List[Tuple[int, int]]:
// Dimi uses i == y and j == x
func getNeighbors(grid [][]int, i int, j int) []aoc.Pos {
	var neighbors = make([]aoc.Pos, 0, 4)
	numRows := len(grid)
	numCols := len(grid[i])

	if i-1 >= 0 {
		neighbors = append(neighbors, aoc.Pos{Y: i - 1, X: j})
	}
	if i+1 < numRows {
		neighbors = append(neighbors, aoc.Pos{Y: i + 1, X: j})
	}
	if j-1 >= 0 {
		neighbors = append(neighbors, aoc.Pos{Y: i, X: j - 1})
	}
	if j+1 < numCols {
		neighbors = append(neighbors, aoc.Pos{Y: i, X: j + 1})
	}
	return neighbors
}

// def get_neighbors_and_values(matrix: List[List[int]],i: int,j: int) -> Tuple[List[Tuple[int, int]], List[int]]:
func GetNeighborsAndValues(grid [][]int, i int, j int) ([]aoc.Pos, []int) {
	neighbors := getNeighbors(grid, i, j)
	vals := aoc.Map(neighbors, func(n aoc.Pos) int {
		return grid[n.Y][n.X]
	})
	return neighbors, vals
}

// def check_if_lower_than_neighbors(matrix: List[List[int]], i: int, j: int) -> bool
func checkIfLowerThanNeighbors(grid [][]int, i int, j int) bool {
	_, neighVals := GetNeighborsAndValues(grid, i, j)
	if grid[i][j] < aoc.Min(neighVals) {
		return true
	}
	return false
}

// def find_minimums(matrix: List[List[int]]) -> Tuple[List[int], List[]
func findMinimums(grid [][]int) ([]int, []aoc.Pos) {
	var mins = make([]int, 0, 100)
	var minPositions = make([]aoc.Pos, 0, 100)
	for i := range grid {
		for j := range grid[0] {
			if checkIfLowerThanNeighbors(grid, i, j) {
				mins = append(mins, grid[i][j])
				minPositions = append(minPositions, aoc.Pos{Y: i, X: j})
			}
		}
	}
	return mins, minPositions
}

// def find_basins(matrix: List[List[int]]) -> List[Set[Tuple[int, int]]]
func findBasins(grid [][]int) []*aoc.Set[aoc.Pos] {
	_, minPositions := findMinimums(grid)
	var basins = make([]*aoc.Set[aoc.Pos], 0, len(minPositions))

	for _, minPos := range minPositions {
		// Dimi: basin = {min_pos}
		basin := aoc.NewSet(minPos)

		// go has no "double ended queue", so we're doing this directly
		// we are assigning it to q to keep the code in sync with dimi's
		// choice of variables
		// Dimi: q = deque([min_pos])
		q := make([]aoc.Pos, 1, 64)
		q[0] = minPos

		// idiomatic go will be slightly different here
		// Dimi: while q:
		for {
			if len(q) == 0 {
				break
			}

			// Dimi: curr = q.popleft()  # bfs
			var curr aoc.Pos
			curr, q = q[0], q[1:]
			// Dimi: curr_val = matrix[curr[0]][curr[1]]
			currVal := grid[curr.Y][curr.X]
			// Dimi: neighbors, neigh_vals = get_neighbors_and_values(matrix, *curr)
			neighbors, neighVals := GetNeighborsAndValues(grid, curr.Y, curr.X)
			// Dimi: for neigh, val in zip(neighbors, neigh_vals):
			for i, neigh := range neighbors {
				val := neighVals[i]
				// Dimi:     if val > curr_val and val != 9 and neigh not in basin:
				if val > currVal && val != 9 && !basin.Contains(neigh) {
					// Dimi:         q.append(neigh)
					q = append(q, neigh)
					// Dimi:         basin.add(neigh)
					basin.Add(neigh)
				}
			}
		}
		// Dimi: basins.append(basin)
		basins = append(basins, basin)
	}
	return basins
}

// find_topk_basins(matrix: List[List[int]], k: int) ->  List[Set[Tuple[int, int]]]
func findTopKBasins(grid [][]int, k int) []*aoc.Set[aoc.Pos] {
	basins := findBasins(grid)
	sort.Slice(basins, func(i, j int) bool { return basins[i].Len() >= basins[j].Len() })
	return basins[:k]
}
