package main

import (
	"fmt"
	"math"
	"time"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

func main() {
	startTime := time.Now()
	subs := aoc.Sort(aoc.IntList(aoc.Input(2021, 7)))

	// Problem 1
	optimal := subs[len(subs)/2]
	fuel := 0
	for _, s := range subs {
		fuel += int(aoc.Abs(s - optimal))
	}
	fmt.Println("Problem 1:", fuel)

	// Problem 2
	// Approximation to start our search, but might be off by 1 because of ???
	optimal = int(math.Round(float64(aoc.Sum(subs)) / float64(len(subs))))
	var outcomes []int
	for i := -1; i < 2; i++ {
		fuel := 0
		for _, s := range subs {
			distance := aoc.Abs(s - optimal + i)
			fuel += (distance * (distance + 1)) / 2
		}
		outcomes = append(outcomes, fuel)
	}
	fmt.Println("Problem 2:", aoc.Sort(outcomes)[0])
	fmt.Println("Total time: ", time.Since(startTime))
}
