package main

import (
	"fmt"
	"time"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

func main() {
	t1 := time.Now()
	raw := aoc.Lines(aoc.Input(2021, 9))

	// to grid
	grid := make(map[aoc.Pos]int)
	for y, line := range raw {
		for x, n := range aoc.Digits(line) {
			grid[aoc.Pos{X: x, Y: y}] = n
		}
	}

	// Problem 1: find points with no lower neighbours
	var lowPos []aoc.Pos
	for pos, val := range grid {
		neighbors := validMoves(pos.Moves(), grid)
		lowestNeighbor := aoc.Min(aoc.Map(neighbors, func(pos aoc.Pos) int { return grid[pos] }))
		if val < lowestNeighbor {
			lowPos = append(lowPos, pos)
		}
	}

	p1 := aoc.Inject(lowPos, 0, func(pos aoc.Pos, acc int) int {
		return acc + 1 + grid[pos]
	})
	fmt.Println("Problem 1: ", p1)

	// Problem 2: find the largest basins
	basins := make(map[aoc.Pos]int)
	for pos, val := range grid {
		if val == 9 {
			continue
		}
		lp := lowestReachablePoint(pos, grid)
		basins[lp] += 1
	}

	var sizes []int
	for _, v := range basins {
		sizes = append(sizes, v)
	}
	sizes = aoc.Sort(sizes)[len(sizes)-3:]
	fmt.Println("Problem 2: ", sizes[0]*sizes[1]*sizes[2])
	fmt.Println("Time: ", time.Since(t1))
}

func validMoves(moves []aoc.Pos, grid map[aoc.Pos]int) []aoc.Pos {
	var valid []aoc.Pos
	for _, pos := range moves {
		if _, ok := grid[pos]; ok {
			valid = append(valid, pos)
		}
	}
	return valid
}

func lowestReachablePoint(pos aoc.Pos, grid map[aoc.Pos]int) aoc.Pos {
	// recursively find lower neighbors until no lower option exists
	lowestNeighbor := lowestNeighbor(pos, grid)
	if grid[pos] <= grid[lowestNeighbor] {
		// I am the lowest point! Declare victory
		return pos
	}
	return lowestReachablePoint(lowestNeighbor, grid)

}

func lowestNeighbor(pos aoc.Pos, grid map[aoc.Pos]int) aoc.Pos {
	neighbors := validMoves(pos.Moves(), grid)
	idx := aoc.MinimumIndex(neighbors, func(e aoc.Pos) int {
		return grid[e]
	})
	return neighbors[idx]
}
