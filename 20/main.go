package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

func main() {
	raw := strings.ReplaceAll(aoc.Input(2021, 20), "#", "1")
	raw = strings.ReplaceAll(raw, ".", "0")
	lines := aoc.Lines(raw)
	key := lines[0]

	g := &grid{
		key:        key,
		content:    lines[2:],
		background: '0',
	}
	g.iter(2)
	fmt.Println("Problem 1:", g.count())
	g.iter(48)
	fmt.Println("Problem 2:", g.count())
}

type grid struct {
	content    []string
	background rune
	key        string
}

func (g *grid) iter(n int) {
	for i := 0; i < n; i++ {
		g.grow()
		newContent := make([]string, len(g.content))
		for y, row := range g.content {
			// newContent[y] = "strings.Repeat(string(g.background), len(g.content[0]))"
			newContent[y] = ""
			for x := range row {
				idx := g.getIndex(x, y)
				newContent[y] = newContent[y] + string(g.key[idx])
			}
		}
		g.content = newContent

		// background may flip, depending on key
		bg := strings.Repeat(string(g.background), 9)
		i, _ := strconv.ParseInt(bg, 2, 16)
		g.background = rune(g.key[i])
	}
}

// add 1 surrounding border of background
func (g *grid) grow() {
	newContent := make([]string, len(g.content)+2)
	l := strings.Repeat(string(g.background), len(g.content[0])+2)
	newContent[0] = l
	newContent[len(g.content)+1] = l
	for y, row := range g.content {
		newContent[y+1] = string(g.background) + row + string(g.background)
	}
	g.content = newContent
}

func (g *grid) count() int {
	return strings.Count(strings.Join(g.content, ""), "1")
}

func (g *grid) getIndex(x, y int) int {
	bin := make([]rune, 9)
	// -1, -1
	if x-1 < 0 || y-1 < 0 {
		bin[0] = g.background
	} else {
		bin[0] = rune(g.content[y-1][x-1])
	}
	// 0, -1
	if y-1 < 0 {
		bin[1] = g.background
	} else {
		bin[1] = rune(g.content[y-1][x])
	}
	// 1, -1
	if y-1 < 0 || x+1 >= len(g.content[0]) {
		bin[2] = g.background
	} else {
		bin[2] = rune(g.content[y-1][x+1])
	}

	// -1, 0
	if x-1 < 0 {
		bin[3] = g.background
	} else {
		bin[3] = rune(g.content[y][x-1])
	}
	// 0, 0
	bin[4] = rune(g.content[y][x])
	// 1, 0
	if x+1 >= len(g.content[0]) {
		bin[5] = g.background
	} else {
		bin[5] = rune(g.content[y][x+1])
	}

	// -1, +1
	if x-1 < 0 || y+1 >= len(g.content) {
		bin[6] = g.background
	} else {
		bin[6] = rune(g.content[y+1][x-1])
	}
	// 0, +1
	if y+1 >= len(g.content) {
		bin[7] = g.background
	} else {
		bin[7] = rune(g.content[y+1][x])
	}
	// 1, +1
	if y+1 >= len(g.content) || x+1 >= len(g.content[0]) {
		bin[8] = g.background
	} else {
		bin[8] = rune(g.content[y+1][x+1])
	}
	i, _ := strconv.ParseInt(string(bin), 2, 16)
	return int(i)
}

func (g *grid) print() {
	for _, row := range g.content {
		fmt.Println(strings.ReplaceAll(strings.ReplaceAll(row, "1", "#"), "0", "."))
	}
	fmt.Println(strings.Repeat("-", len(g.content[0])))
}
