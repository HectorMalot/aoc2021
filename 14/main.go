package main

import (
	"fmt"
	"time"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

var t1 time.Time

type rule struct {
	left  string
	right string
}

func main() {
	t1 = time.Now()
	lines := aoc.Lines(aoc.Input(2021, 14))
	start := lines[0]
	rules := parseRules(lines[2:])

	// new problem 1: 3143
	counts := NewSolver(start, rules, 10)
	var amounts []int
	for _, v := range counts {
		amounts = append(amounts, v)
	}
	fmt.Printf("Max: %d\nMin: %d\nAnswer: %d\n", aoc.Max(amounts), aoc.Min(amounts), aoc.Max(amounts)-aoc.Min(amounts))

	// new problem 2: 4110215602456
	counts = NewSolver(start, rules, 40)
	amounts = []int{}
	for _, v := range counts {
		amounts = append(amounts, v)
	}
	fmt.Printf("Max: %d\nMin: %d\nAnswer: %d\n", aoc.Max(amounts), aoc.Min(amounts), aoc.Max(amounts)-aoc.Min(amounts))
	fmt.Println("Time: ", time.Since(t1))

}

func parseRules(lines []string) map[string]string {
	var rules = make(map[string]string, len(lines))
	for _, line := range lines {
		var left, right string
		fmt.Sscanf(line, "%2s -> %s", &left, &right)
		rules[left] = right
	}
	return rules
}

func NewSolver(start string, rules map[string]string, n int) map[string]int {
	// Convenience
	newRules := make(map[string][]string)
	for k, r := range rules {
		newRules[k] = []string{string(k[0]) + r, r + string(k[1])}
	}

	// Initial counts
	counts := make(map[string]int)
	for i := range start[1:] {
		key := string(start[i : i+2])
		counts[key]++
	}

	// calculate pairs
	for i := 0; i < n; i++ {
		newCounts := make(map[string]int)
		for k, v := range counts {
			for _, pair := range newRules[k] {
				newCounts[pair] += v
			}
		}
		counts = newCounts
	}

	// calculate actual counts
	finalCounts := make(map[string]int)
	last := string(start[len(start)-1])
	for k, v := range counts {
		s1, s2 := string(k[0]), string(k[1])
		finalCounts[s1] += v
		finalCounts[s2] += v
	}
	for k, v := range finalCounts {
		finalCounts[k] = v / 2
	}
	finalCounts[last] += 1
	return finalCounts
}
