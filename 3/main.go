package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

func main() {
	lines := aoc.Lines(aoc.Input(2021, 3))
	max := len(lines)
	gamma := make([]string, len(lines[0]))
	eps := make([]string, len(lines[0]))
	sums := make([]int, len(lines[0]))
	for _, line := range lines {
		for i, c := range line {
			if c == '1' {
				sums[i] += 1
			}
		}
	}

	for i, e := range sums {
		if e < (max / 2) {
			eps[i] = "1"
			gamma[i] = "0"
		} else {
			eps[i] = "0"
			gamma[i] = "1"
		}
	}

	gs := strings.Join(gamma, "")
	es := strings.Join(eps, "")

	gn, _ := strconv.ParseInt(gs, 2, 64)
	en, _ := strconv.ParseInt(es, 2, 64)

	fmt.Println("problem 1:", gn*en)

	// Oxygen rating
	lines2 := lines
	var oxy string
	for i := 0; i < len(lines[0]); i++ {
		lines2 = filter(lines2, i, false)
		fmt.Printf("Filtering digit %d, %d remaining\n", i, len(lines2))
		if len(lines2) == 1 {
			oxy = lines2[0]
			fmt.Printf("Oxygen Rating: %s\n\n", oxy)
		}
	}

	// CO2 scrubber rating
	lines2 = lines
	var co2 string
	for i := 0; i < len(lines[0]); i++ {
		lines2 = filter(lines2, i, true)
		fmt.Printf("Filtering digit %d, %d remaining\n", i, len(lines2))
		if len(lines2) == 1 {
			co2 = lines2[0]
			fmt.Printf("CO2 Scrubber Rating: %s\n\n", co2)
			break
		}
	}

	on, _ := strconv.ParseInt(oxy, 2, 64)
	cn, _ := strconv.ParseInt(co2, 2, 64)
	fmt.Println("problem 2:", on*cn)
}

func mostCommon(lines []string, pos int) string {
	sum := 0
	for _, line := range lines {
		if line[pos] == '1' {
			sum += 1
		}
	}
	if float64(sum) < float64(len(lines))/2.0 {
		return "0"
	}
	return "1"
}

func filter(lines []string, pos int, invert bool) []string {
	search := mostCommon(lines, pos)
	res := []string{}
	for _, line := range lines {
		if (string(line[pos]) == search && !invert) || (string(line[pos]) != search && invert) {
			res = append(res, line)
		}
	}
	return res
}
