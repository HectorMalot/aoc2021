package main

import (
	"fmt"
	"time"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

func main() {
	t1 := time.Now()
	lines := aoc.Lines(aoc.Input(2021, 15))
	cave := parse(lines)
	fmt.Printf("Problem 1: %d (finished in %s)\n", Solve(cave), time.Since(t1))

	t2 := time.Now()
	cave2 := growCave(cave, 5)
	fmt.Printf("Problem 2: %d (finished in %s)\n", Solve(cave2), time.Since(t2))
}

func parse(lines []string) [][]int {
	var cave = make([][]int, len(lines))
	for i, line := range lines {
		cave[i] = aoc.Digits(line)
	}
	return cave
}

func printCave(in [][]int) {
	for _, l := range in {
		fmt.Println(l)
	}
}

func Solve(cave [][]int) int {
	cave[0][0] = 0 // Starting position does not count

	// Create tracker for lowest feasible path
	var scores = make([][]int, len(cave))
	for j := range scores {
		row := []int{}
		for i := 0; i < len(cave[0]); i++ {
			row = append(row, 65535)
		}
		scores[j] = row
	}
	scores[0][0] = 0

	var changed bool
	for i := 0; i < len(cave)+len(cave[0]); i++ {
		scores, changed = iterateScores(cave, scores)
		if !changed {
			fmt.Printf("Stabilized in %d iterations\n", i)
			break
		}
	}
	return scores[len(scores)-1][len(scores[0])-1]
}

//	if my neighbor_score + own value <  current_score => current_score = nb+self_value
func iterateScores(cave, scores [][]int) ([][]int, bool) {
	changed := false
	maxY, maxX := len(cave)-1, len(cave[0])-1
	for y, row := range cave {
		for x, v := range row {
			// top
			if y > 0 && v+scores[y-1][x] < scores[y][x] {
				scores[y][x] = v + scores[y-1][x]
				changed = true
			}
			// bottom
			if y < maxY && v+scores[y+1][x] < scores[y][x] {
				scores[y][x] = v + scores[y+1][x]
				changed = true
			}
			// left
			if x > 0 && v+scores[y][x-1] < scores[y][x] {
				scores[y][x] = v + scores[y][x-1]
				changed = true
			}
			// right
			if x < maxX && v+scores[y][x+1] < scores[y][x] {
				scores[y][x] = v + scores[y][x+1]
				changed = true
			}
		}
	}
	return scores, changed
}

/*
New tiles: all of its risk levels are 1 higher than the tile immediately up or left of it. However, risk levels above 9 wrap back around to 1
*/
func growCave(cave [][]int, n int) [][]int {
	yLen, xLen := len(cave), len(cave[0])
	newCave := make([][]int, yLen*n)
	for y := range newCave {
		newCave[y] = make([]int, xLen*n)
	}

	for i := 0; i < n; i++ { // Y
		for j := 0; j < n; j++ { // X
			for y, row := range cave {
				for x, v := range row {
					newCave[y+yLen*i][x+xLen*j] = ((v + i + j - 1) % 9) + 1
				}
			}
		}
	}

	return newCave
}
