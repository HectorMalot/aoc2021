package main

import (
	"fmt"
	"testing"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

func TestAll(t *testing.T) {
	input := `1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581`
	cave := parse(aoc.Lines(input))
	cave = growCave(cave, 5)
	printCave(cave)
	t.FailNow()

}

func TestDimi(t *testing.T) {
	input := `1911191111
1119111991
9999999111
9999911199
9999119999
9999199999
9111199999
9199999111
9111911191
9991119991`
	cave := parse(aoc.Lines(input))
	fmt.Println(Solve(cave))
	t.FailNow()
}
