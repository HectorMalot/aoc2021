package main

import (
	"fmt"
	"time"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

func main() {
	// x=150..171, y=-129..-70
	t := target{
		xmin: 150,
		xmax: 171,
		ymin: -129,
		ymax: -70,
	}
	cnt := bruteforce(t)
	fmt.Printf("Velocities: %d\n", cnt)
}

// Solve works on the example input, not on the challenge...
func Solve(t target) int {
	minY, maxY := t.ymin-5, aoc.Abs(t.ymin)+5
	velocities := aoc.NewSet[string]()

	for yVel := minY; yVel < maxY; yVel++ { // for MaxY..MinY -> Y
		// - Determine steps where Y is in the window (e.g. steps 4 - 7)
		var minS, maxS int
		for s := 1; s < maxY*2; s++ {
			yPos := Y(yVel, s)
			if yPos > t.ymax { // if above: continue (wait till it falls into range)
				continue
			}
			if yPos < t.ymin { // if below: set max, break
				maxS = s
				if minS == 0 {
					minS = 1 // Being careful
				}
				break
			}
			// If we reach this branch, we're in the box from the Y perspective
			if minS == 0 { // if in for first time: set min
				minS = s - 1
				// fmt.Println("setting minS to:", s)
			}
		}
		// Relevant X velocities for this Y velocity + steps
		for s := minS - 1; s < maxS+1; s++ {
			minXv, maxXv := minXvel(t.xmin, s)-10, maxXvel(t.xmax, s)+10
			for xVel := minXv; xVel < maxXv; xVel++ {
				if t.Contains(X(xVel, s), Y(yVel, s)) {
					velocities.Add(fmt.Sprintf("%d,%d", xVel, yVel))
				}
			}
		}
	}

	return velocities.Len()
}

func bruteforce(t target) int {
	t1 := time.Now()
	velocities := aoc.NewSet[string]()
	for x := 1; x <= t.xmax; x++ {
		for y := t.ymin; y <= aoc.Abs(t.ymin)*3; y++ {
			for s := 1; s < aoc.Abs(t.ymin)*3; s++ {
				if t.Contains(X(x, s), Y(y, s)) {
					velocities.Add(fmt.Sprintf("%d,%d", x, y))
				}
			}
		}
	}
	fmt.Println(time.Since(t1))
	return velocities.Len()
}

type target struct {
	xmin, xmax, ymin, ymax int
}

func (t target) Contains(x, y int) bool {
	if x >= t.xmin && x <= t.xmax && y >= t.ymin && y <= t.ymax {
		return true
	}
	return false
}

func X(vel, steps int) int {
	if vel < 0 {
		return 0
	}
	if steps > vel {
		return X(vel, vel)
	}
	return ((2*vel - steps + 1) * steps) / 2
}

func Y(vel, steps int) int {
	return ((2*vel - steps + 1) * steps) / 2
}

func minXvel(distance, steps int) int {
	if steps == 0 {
		return 0
	}
	if (2*distance-steps+steps*steps)%(2*steps) == 0 {
		return maxXvel(distance, steps)
	}
	return maxXvel(distance, steps) + 1
}

func maxXvel(distance, steps int) int {
	if steps == 0 {
		return 0
	}
	return (2*distance - steps + steps*steps) / (2 * steps)
}
