package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestYvelocity(t *testing.T) {
	tests := []struct {
		in  int
		out int
	}{
		{in: 1, out: 5},
		{in: 3, out: 12},
		{in: 5, out: 15},
		{in: 6, out: 15},
		{in: 7, out: 14},
		{in: 8, out: 12},
	}
	for _, test := range tests {
		require.Equal(t, test.out, Y(5, test.in))
	}
}

func TestXvelocity(t *testing.T) {
	tests := []struct {
		in  int
		vel int
		out int
	}{
		{in: 1, vel: 5, out: 5},
		{in: 3, vel: 5, out: 12},
		{in: 5, vel: 5, out: 15},
		{in: 6, vel: 5, out: 15},
		{in: 7, vel: 5, out: 15},
		{in: 8, vel: 5, out: 15},
		{in: 5, vel: 20, out: 90},
	}
	for _, test := range tests {
		require.Equal(t, test.out, X(test.vel, test.in))
	}
}

func TestMinXvelocity(t *testing.T) {
	tests := []struct {
		steps    int
		distance int
		out      int
	}{
		{steps: 4, distance: 149, out: 39},
		{steps: 4, distance: 150, out: 39},
		{steps: 4, distance: 151, out: 40},
	}
	for _, test := range tests {
		require.Equal(t, test.out, minXvel(test.distance, test.steps))
	}
}

func TestMaxXvelocity(t *testing.T) {
	tests := []struct {
		steps    int
		distance int
		out      int
		msg      string
	}{
		{steps: 4, distance: 149, out: 38, msg: "just before equal"},
		{steps: 4, distance: 150, out: 39, msg: "equal"},
		{steps: 4, distance: 151, out: 39, msg: "just after equal"},
	}
	for _, test := range tests {
		require.Equal(t, test.out, maxXvel(test.distance, test.steps), test.msg)
	}
}

func TestE2E(t *testing.T) {
	// target area: x=20..30, y=-10..-5
	tgt := target{
		xmin: 20,
		xmax: 30,
		ymin: -10,
		ymax: -5,
	}
	cnt := Solve(tgt)
	fmt.Printf("Velocities: %d\n", cnt)
	require.Equal(t, 112, cnt)
	t.FailNow()
}
