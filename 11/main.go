package main

import (
	"fmt"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

func main() {
	lines := aoc.Lines(aoc.Input(2021, 11))
	problem1(lines)
	lines = aoc.Lines(`5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526`)
	problem2(lines)
}

type octo struct {
	pos        aoc.Pos
	neighbors  []*octo
	value      int
	hasFlashed bool
	flashes    int
}

func problem1(lines []string) {
	_, octos := parse(lines)
	// Problem 1
	for i := 0; i < 100; i++ {
		// Add 1 to each octo
		// Automatically cascading flashes
		for _, o := range octos {
			o.Add(1)
		}
		// Reset for the next round
		for _, o := range octos {
			o.Reset()
		}
	}
	fmt.Println("Problem 1: ", aoc.Inject(octos, 0, func(o *octo, acc int) int { return acc + o.flashes }))
}

func problem2(lines []string) {
	_, octos := parse(lines)
	for i := 1; i > 0; i++ {
		// Add 1 to each octo
		// Automatically cascading flashes
		for _, o := range octos {
			o.Add(1)
		}
		// Determine if we have a hit
		if aoc.Inject(octos, 0, func(o *octo, acc int) int {
			if o.hasFlashed {
				return acc + 1
			}
			return acc
		}) == len(octos) {
			fmt.Println("Problem 2: ", i)
			return
		}
		// Reset for the next round
		for _, o := range octos {
			o.Reset()
		}
	}
}

func (o *octo) AddNeighbor(other *octo) {
	o.neighbors = append(o.neighbors, other)
}

func (o *octo) Add(amount int) {
	if o.hasFlashed {
		return
	}
	o.value += amount
	if o.value > 9 {
		o.Flash()
	}
}

func (o *octo) Flash() {
	if o.hasFlashed {
		return
	}
	o.hasFlashed = true
	o.flashes++
	for _, n := range o.neighbors {
		n.Add(1)
	}
}

func (o *octo) Reset() {
	if o.hasFlashed {
		o.value = 0
		o.hasFlashed = false
	}
}

func parse(lines []string) (map[aoc.Pos]*octo, []*octo) {
	octos := make([]*octo, 0, len(lines)*len(lines[0]))
	grid := make(map[aoc.Pos]*octo)
	for y, line := range lines {
		for x, d := range aoc.Digits(line) {
			pos := aoc.Pos{X: x, Y: y}
			octopus := &octo{pos: pos, value: d, neighbors: make([]*octo, 0, 8)}
			octos = append(octos, octopus)
			grid[pos] = octopus
		}
	}

	// pre-load neighbors
	for pos, o := range grid {
		neighbors := pos.Numpad()
		for _, n := range neighbors {
			if o2, ok := grid[n]; ok {
				o.AddNeighbor(o2)
			}
		}
	}

	return grid, octos
}
