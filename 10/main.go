package main

import (
	"fmt"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

var (
	lines  []string
	open   []rune
	close  []rune
	pairs  map[rune]rune
	scores map[rune]int
)

func init() {
	lines = aoc.Lines(aoc.Input(2021, 10))
	open = []rune{'(', '[', '{', '<'}
	close = []rune{')', ']', '}', '>'}
	pairs = make(map[rune]rune)
	scores = make(map[rune]int)

	for i, r := range open {
		pairs[r] = close[i]
		pairs[close[i]] = r
	}

	scores['0'] = 0     // 0: default if no syntax error
	scores[')'] = 3     // ): 3 points.
	scores[']'] = 57    // ]: 57 points.
	scores['}'] = 1197  // }: 1197 points.
	scores['>'] = 25137 // >: 25137 points.

}

func main() {
	// Problem 1: corrupted
	var incomplete []string
	var score int
	for _, line := range lines {
		r := solveLine(line)
		score += scores[r]
		if r == '0' {
			incomplete = append(incomplete, line)
		}
	}
	fmt.Println("Problem 1: ", score)

	// Problem 2: incompletes
	var scores []int
	for _, line := range incomplete {
		sol := completeLine(line)
		scores = append(scores, scoreSolution(sol))
	}
	scores = aoc.Sort(scores)
	fmt.Println("Prolem 2: ", scores[len(scores)/2])
}

func solveLine(line string) rune {
	var stack []rune
	for _, r := range line {
		if aoc.Contains(open, r) {
			stack = append(stack, r)
			continue
		}
		if stack[len(stack)-1] == pairs[r] {
			stack = stack[:len(stack)-1] // pop last element
			continue
		}
		return r
	}
	return '0'
}

// Assumes an incomplete line!
func completeLine(line string) []rune {
	var stack []rune
	for _, r := range line {
		if aoc.Contains(open, r) {
			stack = append(stack, r)
			continue
		}
		if stack[len(stack)-1] == pairs[r] {
			stack = stack[:len(stack)-1] // pop last element
			continue
		}
	}
	completion := make([]rune, len(stack))
	for i, r := range stack {
		completion[len(completion)-1-i] = pairs[r]
	}

	return completion
}

func scoreSolution(sol []rune) int {
	scores := map[rune]int{')': 1, ']': 2, '}': 3, '>': 4}
	score := 0
	for _, r := range sol {
		score = score*5 + scores[r]
	}
	return score
}
