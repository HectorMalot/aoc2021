package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/hectormalot/aoc2021/aoc"
)

func TestLineSolver(t *testing.T) {
	inputCorrupted := "{([(<{}[<>[]}>{[]{[(<()>"
	r := solveLine(inputCorrupted)
	require.Equal(t, '}', r)

	inputIncomplete := "[({(<(())[]>[[{[]{<()<>>"
	r = solveLine(inputIncomplete)
	require.Equal(t, '0', r)

}

func TestCompletionSolver(t *testing.T) {
	inputIncomplete := "[({(<(())[]>[[{[]{<()<>>"
	expected := []rune{'}', '}', ']', ']', ')', '}', ')', ']'}
	res := completeLine(inputIncomplete)
	require.Equal(t, expected, res)

	val := scoreSolution(res)
	require.Equal(t, 288957, val)
}

func TestDimi(t *testing.T) {
	q := []aoc.Pos{{X: 3, Y: 4}}

	// go's range over array is equal to popleft until done
	// Dimi: q = deque([min_pos])
	// Dimi: while q:
	// Dimi:     curr = q.popleft()  # bfs
	for i, pos := range q {
		fmt.Printf("%d - %v", i, pos)
		if i == 0 {
			q = append(q, aoc.Pos{X: 9, Y: 9})
		}
		fmt.Println(q)
	}
	t.FailNow()
}
