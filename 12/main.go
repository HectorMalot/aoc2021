package main

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

func main() {
	links := getLinks(aoc.Lines(aoc.Input(2021, 12)))
	start := time.Now()
	fmt.Println("Possible paths problem 1:", len(problem1(links)), time.Since(start))
	start = time.Now()
	fmt.Println("Possible paths problem 2:", len(problem2(links)), time.Since(start))
}

func getLinks(lines []string) map[string][]string {
	links := make(map[string][]string)
	for _, line := range lines {
		split := strings.Split(line, "-")
		from, to := split[0], split[1]
		links[from] = append(links[from], to)
		links[to] = append(links[to], from)
	}
	return links
}

func problem1(links map[string][]string) [][]string {
	filterFn := func(curPath []string) func(string) bool {
		return func(e string) bool {
			if aoc.Contains(curPath, strings.ToLower(e)) {
				return false
			}
			return true
		}
	}
	return findPaths([]string{"start"}, links, filterFn)
}

func problem2(links map[string][]string) [][]string {
	filterFn := func(curPath []string) func(string) bool {
		return func(e string) bool {
			if aoc.Contains(curPath, strings.ToLower(e)) && visitedSmallCaveTwice(curPath) || e == "start" {
				return false
			}
			return true
		}
	}
	return findPaths([]string{"start"}, links, filterFn)
}

func findPaths(curPath []string, links map[string][]string, filterFn func(curPath []string) func(string) bool) [][]string {
	curPos := curPath[len(curPath)-1]
	options := aoc.Filter(links[curPos], filterFn(curPath)) // Possible moves

	// Recursively find paths
	paths := make([][]string, 0, 20)
	for _, o := range options {
		p := append(curPath, o)
		if o == "end" {
			paths = append(paths, p)
			continue
		}
		paths = append(paths, findPaths(p, links, filterFn)...)
	}
	return paths
}

func visitedSmallCaveTwice(path []string) bool {
	smallCaves := aoc.Filter(path, func(e string) bool {
		return e == strings.ToLower(e)
	})
	count := make(map[string]int)
	for _, c := range smallCaves {
		count[c]++
		if count[c] > 1 {
			return true
		}
	}
	return false
}
