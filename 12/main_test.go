package main

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/hectormalot/aoc2021/aoc"
)

func TestSmallInput(t *testing.T) {
	in := `start-A
start-b
A-c
A-b
b-d
A-end
b-end`

	paths := problem1(getLinks(aoc.Lines(in)))
	require.Equal(t, 10, len(paths))

	paths = problem2(getLinks(aoc.Lines(in)))
	require.Equal(t, 36, len(paths))
}

func TestMediumInput(t *testing.T) {
	in := `dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc`

	paths := problem1(getLinks(aoc.Lines(in)))
	require.Equal(t, 19, len(paths))
	paths = problem2(getLinks(aoc.Lines(in)))
	require.Equal(t, 103, len(paths))

}

func TestLargeInput(t *testing.T) {
	in := `fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW`

	paths := problem1(getLinks(aoc.Lines(in)))
	require.Equal(t, 226, len(paths))
	paths = problem2(getLinks(aoc.Lines(in)))
	require.Equal(t, 3509, len(paths))

}

func TestDimi(t *testing.T) {
	in := `ma-start
YZ-rv
MP-rv
vc-MP
QD-kj
rv-kj
ma-rv
YZ-zd
UB-rv
MP-xe
start-MP
zd-end
ma-UB
ma-MP
UB-xe
end-UB
ju-MP
ma-xe
zd-UB
start-xe
YZ-end`

	paths := problem1(getLinks(aoc.Lines(in)))
	require.Equal(t, 4549, len(paths))
	paths = problem2(getLinks(aoc.Lines(in)))
	require.Equal(t, 120535, len(paths))
}
