package main

import (
	"fmt"
	"strings"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

type fold struct {
	dir   string
	pivot int
}

func main() {
	pos, folds := parse()

	// Problem 1:
	board := newBoard(pos)
	board = foldBoard(board, folds[0])
	fmt.Println("Problem 1:", len(board))

	// Problem 2:
	board = newBoard(pos)
	for _, f := range folds {
		board = foldBoard(board, f)
	}
	maxX, maxY := 0, 0
	for p := range board {
		maxX = aoc.Max([]int{maxX, p.X})
		maxY = aoc.Max([]int{maxY, p.Y})
	}
	lb := aoc.NewLightBoard(maxX, maxY)
	for p, v := range board {
		lb.Set(p.X, p.Y, v)
	}
	fmt.Println("Problem 2:")
	lb.Print()
}

func parse() ([]aoc.Pos, []fold) {
	var pos []aoc.Pos
	var folds []fold
	lines := aoc.Lines(aoc.Input(2021, 13))
	for _, line := range lines {
		switch {
		case line == "":
			continue
		case strings.Contains(line, "fold"):
			// fold along x=655
			var dir string
			var pivot int
			fmt.Sscanf(line, "fold along %1s=%d", &dir, &pivot)
			folds = append(folds, fold{dir: dir, pivot: pivot})
			continue
		default:
			var x, y int
			fmt.Sscanf(line, "%d,%d", &x, &y)
			pos = append(pos, aoc.Pos{X: x, Y: y})
		}
	}
	return pos, folds
}

func newBoard(pos []aoc.Pos) map[aoc.Pos]bool {
	b := make(map[aoc.Pos]bool)
	for _, p := range pos {
		b[p] = true
	}
	return b
}

func foldBoard(b map[aoc.Pos]bool, f fold) map[aoc.Pos]bool {
	if f.dir == "x" {
		for p := range b {
			if p.X > f.pivot {
				delete(b, p)
				p.X = 2*f.pivot - p.X
				b[p] = true
			}
		}
	} else if f.dir == "y" {
		for p := range b {
			if p.Y > f.pivot {
				delete(b, p)
				p.Y = 2*f.pivot - p.Y
				b[p] = true
			}
		}
	}
	return b
}
