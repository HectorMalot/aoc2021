package main

import (
	"fmt"
	"os"
	"runtime/pprof"
	"time"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

func main() {
	f, _ := os.Create("cpu.pprof")
	pprof.StartCPUProfile(f)
	ch := time.After(10 * time.Second)
	go func(stop <-chan time.Time) {
		<-stop
		pprof.StopCPUProfile()
		fmt.Println("done profiling")
	}(ch)
	tStart := time.Now()
	lines := aoc.Lines(aoc.Input(2021, 19))
	scanners := parse(lines)
	probes, scanners := Solve(scanners)
	fmt.Println("Problem 1:", probes)

	dist := []int{}
	for _, s1 := range scanners {
		for _, s2 := range scanners {
			if s1.id == s2.id {
				continue
			}
			dist = append(dist, s1.origin.ManhattanDist(s2.origin))
		}
	}
	fmt.Println("Problem 2:", aoc.Max(dist))
	fmt.Println("Solved in:", time.Since(tStart)) // approx 3 minutes
}

type scanner struct {
	id     int
	probes *aoc.Set[aoc.Pos3D]
	origin aoc.Pos3D // origin, default 0
	pjs    []*scanner
}

func newScanner() *scanner {
	return &scanner{probes: aoc.NewSet[aoc.Pos3D](), origin: aoc.Pos3D{}}
}

func Solve(scanners []*scanner) (int, []*scanner) {
	grid := scanners[0] // scanner 0 is going to be our starting point
	search := scanners[1:]
	res := []*scanner{}

	for len(search) != 0 {
		if s, ok := grid.match(search[0]); ok {
			grid = grid.merge(s)
			res = append(res, s)
			fmt.Printf("found match for scanner: %2d, Grid now has %2d probes\n", s.id, grid.probes.Len())
			if len(search) == 1 {
				fmt.Println("matched last scanner")
				break // Stop loop on last match
			}
			search = search[1:]
			continue
		}
		// no match... yet! move to end of queue
		fmt.Printf("Failed to match scanner %2d, moving to end of queue. %2d scanners remain unmatched\n", search[0].id, len(search))
		search = append(search[1:], search[0])
	}

	return grid.probes.Len(), res
}

func parse(lines []string) []*scanner {
	var s *scanner
	scanners := []*scanner{}
	for _, line := range lines {
		if len(line) == 0 {
			continue
		}
		if line[1] == '-' {
			s = newScanner()
			fmt.Sscanf(line, "--- scanner %d ---", &s.id)
			scanners = append(scanners, s)
			continue
		}
		c := aoc.IntList(line)
		s.addProbe(c[0], c[1], c[2])
	}
	return scanners
}

func (s *scanner) addProbe(x, y, z int) {
	s.probes.Add(aoc.Pos3D{X: x, Y: y, Z: z})
}

// returns all 24 projections
func (s *scanner) projections() []*scanner {
	if s.pjs != nil {
		return s.pjs
	}
	scanners := make([]*scanner, 0, 24)
	// y rotations
	for y := 0; y < 4; y++ {
		for x := 0; x < 4; x++ {
			scanners = append(scanners, s.rotate(x, y, 0))
		}
	}
	// z rotations
	for x := 0; x < 4; x++ {
		scanners = append(scanners, s.rotate(x, 0, 1))
		scanners = append(scanners, s.rotate(x, 0, 3))
	}

	s.pjs = scanners // cache
	return scanners
}

// rotates in 90 degree increments in order of x, then y, then z
// returns a NEW scanner, rotates around 0,0,0!
func (s *scanner) rotate(x, y, z int) *scanner {
	s2 := newScanner()
	s2.id = s.id
	for _, probe := range s.probes.Elems() {
		s2.probes.Add(probe.Rotate(x, y, z))
	}
	s2.origin = s.origin.Rotate(x, y, z)
	return s2
}

// moves scanner in space, including probes
func (s *scanner) move(x, y, z int) {
	s.origin = s.origin.Add(x, y, z)
	newProbes := aoc.NewSet[aoc.Pos3D]()
	for _, p := range s.probes.Elems() {
		newProbes.Add(p.Add(x, y, z))
	}
	s.probes = newProbes
}

// Checks if a scanner matches another scanner (will also do movements and rotations)
// returns true if match is found, in which case it also returns a scanner
// in the right orientation and position, otherwise returns false
func (s *scanner) match(other *scanner) (*scanner, bool) {
	s2projections := other.projections()
	for _, s2 := range s2projections {
		for _, p1 := range s.probes.Elems() {
			for _, p2 := range s2.probes.Elems() {
				// Move p2 on top of p1 and Match away!
				rel := p1.Rel(p2)
				s2.move(rel.X, rel.Y, rel.Z)
				if s.overlap(s2) {
					return s2, true
				}
				s2.move(-rel.X, -rel.Y, -rel.Z)
			}
		}
	}
	return nil, false
}

// Merges the 2 scanners into 1
func (s *scanner) merge(other *scanner) *scanner {
	s.probes = s.probes.Union(other.probes)
	return s
}

// returns true if overlapping probes for 2 scanners >= 12 (does not move/rotate)
func (s *scanner) overlap(other *scanner) bool {
	ol := s.probes.Intersection(other.probes).Len()
	return ol >= 12
}
