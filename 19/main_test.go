package main

import (
	"bufio"
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/hectormalot/aoc2021/aoc"
)

func TestRelativeMove(t *testing.T) {
	s1 := newScanner()
	s1.addProbe(10, 20, 30)
	s2 := newScanner()
	s2.addProbe(11, 22, 33)

	p1, p2 := s1.probes.Elems()[0], s2.probes.Elems()[0]
	rel := p1.Rel(p2)
	fmt.Println(rel)
	s2.move(rel.X, rel.Y, rel.Z)
	require.Equal(t, aoc.Pos3D{X: -1, Y: -2, Z: -3}, s2.origin)
	require.Equal(t, aoc.Pos3D{X: 10, Y: 20, Z: 30}, s2.probes.Elems()[0])
}

func TestExample(t *testing.T) {
	input, _ := ImportToStringSlice("day19_testinput.txt")
	scanners := parse(input)
	num, _ := Solve(scanners)
	require.Equal(t, 79, num)
}

// ImportToStringSlice opens a whole file into memory and returns the content
// as a slice of strings
func ImportToStringSlice(path string) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}
	return lines, scanner.Err()
}
