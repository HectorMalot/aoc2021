package main

import (
	"fmt"
	"time"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

type Line struct {
	from aoc.Pos
	to   aoc.Pos
}

func main() {
	t1 := time.Now()
	ages := aoc.IntList(aoc.Input(2021, 6))
	world := make([]int, 9)
	idx := 0
	N := 256

	for _, n := range ages {
		world[n]++
	}

	for i := 0; i < N; i++ {
		world[idx], world[7], world[8] = world[idx]+world[7], world[8], world[idx] // Reproduction
		idx = (idx + 1) % 7                                                        // Move the 0 position
	}

	fmt.Println(aoc.Sum(world))
	fmt.Println(time.Since(t1))
}
