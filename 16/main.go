package main

import (
	"fmt"
	"strconv"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

func main() {
	input := aoc.Input(2021, 16)
	binString := hexToBinstring(input)
	packets := parsePacket(binString)

	fmt.Println("Problem 1:", packets.VersionSum())
	fmt.Println("Problem 2:", packets.Exec())
}

type Packet struct {
	Version      int
	Type         int
	OrdinalValue int
	Len          int
	Children     []*Packet

	raw string
}

func hexToBinstring(input string) string {
	bs := ""
	for _, c := range input {
		v, _ := strconv.ParseUint(string(c), 16, 16)
		bs = fmt.Sprintf("%s%04b", bs, v)
	}
	return bs
}

func parsePacket(input string) *Packet {
	p := &Packet{}
	p.raw = input
	p.Version = aoc.BinAtoi(input[0:3])
	p.Type = aoc.BinAtoi(input[3:6])

	// Handle ordinal values
	if p.Type == 4 {
		curPos := 6
		binVal := ""
		for {
			// if not last frame, handle and continue
			if p.raw[curPos] == '1' {
				binVal = fmt.Sprintf("%s%s", binVal, p.raw[curPos+1:curPos+5])
				curPos += 5
				continue
			}
			// if last frame, handle and break
			binVal = fmt.Sprintf("%s%s", binVal, p.raw[curPos+1:curPos+5])
			p.Len = curPos + 5
			break
		}
		p.OrdinalValue = aoc.BinAtoi(binVal)
		return p
	}

	// operator packets
	if p.raw[6] == '0' { // next 15 bits are number for length of the packet
		l := aoc.BinAtoi(p.raw[7 : 7+15])
		parsedlen := 0
		for parsedlen < l {
			sub := parsePacket(p.raw[7+15+parsedlen:])
			parsedlen += sub.Len
			p.Children = append(p.Children, sub)
		}
		p.Len = parsedlen + 7 + 15

	}

	if p.raw[6] == '1' { // next 11 bits are the number of subpackages
		n := aoc.BinAtoi(p.raw[7 : 7+11])
		parsedpkg := 0
		parsedlen := 0
		for parsedpkg < n {
			sub := parsePacket(p.raw[7+11+parsedlen:])
			parsedlen += sub.Len
			parsedpkg++
			p.Children = append(p.Children, sub)
		}
		p.Len = parsedlen + 7 + 11
	}

	return p
}

func (p *Packet) VersionSum() int {
	return aoc.Inject(p.Children, p.Version, func(p *Packet, acc int) int { return acc + p.VersionSum() })
}

func (p *Packet) PackageCount() int {
	return aoc.Inject(p.Children, 1, func(p *Packet, acc int) int { return acc + p.PackageCount() })
}

/*
Packets with type ID 0 are sum packets - their value is the sum of the values of their sub-packets. If they only have a single sub-packet, their value is the value of the sub-packet.
Packets with type ID 1 are product packets - their value is the result of multiplying together the values of their sub-packets. If they only have a single sub-packet, their value is the value of the sub-packet.
Packets with type ID 2 are minimum packets - their value is the minimum of the values of their sub-packets.
Packets with type ID 3 are maximum packets - their value is the maximum of the values of their sub-packets.
Packets with type ID 5 are greater than packets - their value is 1 if the value of the first sub-packet is greater than the value of the second sub-packet; otherwise, their value is 0. These packets always have exactly two sub-packets.
Packets with type ID 6 are less than packets - their value is 1 if the value of the first sub-packet is less than the value of the second sub-packet; otherwise, their value is 0. These packets always have exactly two sub-packets.
Packets with type ID 7 are equal to packets - their value is 1 if the value of the first sub-packet is equal to the value of the second sub-packet; otherwise, their value is 0. These packets always have exactly two sub-packets.
*/
func (p *Packet) Exec() int {
	switch p.Type {
	case 0:
		return aoc.Inject(p.Children, 0, func(p *Packet, acc int) int { return acc + p.Exec() })
	case 1:
		return aoc.Inject(p.Children, 1, func(p *Packet, acc int) int { return acc * p.Exec() })
	case 2:
		return aoc.Inject(p.Children, 1000000, func(p *Packet, acc int) int { return aoc.Min([]int{acc, p.Exec()}) })
	case 3:
		return aoc.Inject(p.Children, 0, func(p *Packet, acc int) int { return aoc.Max([]int{acc, p.Exec()}) })
	case 4:
		return p.OrdinalValue
	case 5:
		if p.Children[0].Exec() > p.Children[1].Exec() {
			return 1
		}
		return 0
	case 6:
		if p.Children[0].Exec() < p.Children[1].Exec() {
			return 1
		}
		return 0
	case 7:
		if p.Children[0].Exec() == p.Children[1].Exec() {
			return 1
		}
		return 0
	}
	return 0
}
