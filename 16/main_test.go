package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestVarious(t *testing.T) {
	var val uint8 = 0b00000100
	fmt.Printf("%08b\n", val)
	fmt.Printf("%08b\n", val<<1)
	fmt.Printf("%08b\n", val>>1)

	val = 0b11111111
	fmt.Printf("%08b\n", val<<1)
	fmt.Printf("%08b\n", val>>1)

	// get first 3
	val = 0b10100000
	val = val >> (8 - 3)
	fmt.Printf("%08b\n", val)

	// get middle 4 (post (2:6))
	val = 0b00111100
	val = val << 2
	val = val >> (8 - 6 + 2)
	fmt.Printf("%08b\n", val)

}

func TestExample1(t *testing.T) {
	/*
		D2FE28
		110100101111111000101000
		VVVTTTAAAAABBBBBCCCCC
		HHHHHHBBBBBBBBBBBBBBB
	*/
	hex := "D2FE28"
	bins := hexToBinstring(hex)
	require.Equal(t, "110100101111111000101000", bins)
	packet := parsePacket(bins)
	fmt.Printf("%+#v", packet)
	require.Equal(t, 2021, packet.OrdinalValue)
}

func TestExample2(t *testing.T) {
	// 38006F45291200
	// 00111000000000000110111101000101001010010001001000000000
	// VVVTTTILLLLLLLLLLLLLLLAAAAAAAAAAABBBBBBBBBBBBBBBB
	hex := "38006F45291200"
	bins := hexToBinstring(hex)
	require.Equal(t, "00111000000000000110111101000101001010010001001000000000", bins)
	packet := parsePacket(bins)
	require.Equal(t, 10, packet.Children[0].OrdinalValue)
	require.Equal(t, 20, packet.Children[1].OrdinalValue)

	// As another example, here is an operator packet (hexadecimal string EE00D40C823060) with length type ID 1 that contains three sub-packets:
	// 11101110000000001101010000001100100000100011000001100000
	// VVVTTTILLLLLLLLLLLAAAAAAAAAAABBBBBBBBBBBCCCCCCCCCCC
	// The three bits labeled V (111) are the packet version, 7.
	// The three bits labeled T (011) are the packet type ID, 3, which means the packet is an operator.
	// The bit labeled I (1) is the length type ID, which indicates that the length is a 11-bit number representing the number of sub-packets.
	// The 11 bits labeled L (00000000011) contain the number of sub-packets, 3.
	// The 11 bits labeled A contain the first sub-packet, a literal value representing the number 1.
	// The 11 bits labeled B contain the second sub-packet, a literal value representing the number 2.
	// The 11 bits labeled C contain the third sub-packet, a literal value representing the number 3.
	hex = "EE00D40C823060"
	bins = hexToBinstring(hex)
	require.Equal(t, "11101110000000001101010000001100100000100011000001100000", bins)
	packet = parsePacket(bins)
	require.Equal(t, 1, packet.Children[0].OrdinalValue)
	require.Equal(t, 2, packet.Children[1].OrdinalValue)
	require.Equal(t, 3, packet.Children[2].OrdinalValue)

}

func TestExamples3(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{in: "8A004A801A8002F478", out: 16},
		{in: "620080001611562C8802118E34", out: 12},
		{in: "C0015000016115A2E0802F182340", out: 23},
		{in: "A0016C880162017C3686B18A3D4780", out: 31},
	}

	for _, test := range tests {
		fmt.Println(test.in)
		bins := hexToBinstring(test.in)
		p := parsePacket(bins)
		fmt.Println("parsedpacket")
		require.Equal(t, test.out, p.VersionSum(), test.in)
	}

}

func TestExamples4(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{in: "C200B40A82", out: 3},
		{in: "04005AC33890", out: 54},
		{in: "880086C3E88112", out: 7},
		{in: "CE00C43D881120", out: 9},
		{in: "D8005AC2A8F0", out: 1},
		{in: "F600BC2D8F", out: 0},
		{in: "9C005AC2F8F0", out: 0},
		{in: "9C0141080250320F1802104A08", out: 1},
	}

	for _, test := range tests {
		fmt.Println(test.in)
		bins := hexToBinstring(test.in)
		p := parsePacket(bins)
		require.Equal(t, test.out, p.Exec(), test.in)
	}

}
