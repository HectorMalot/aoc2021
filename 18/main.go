package main

import (
	"bufio"
	"fmt"
	"strings"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

func main() {
	lines := aoc.Lines(aoc.Input(2021, 18))
	var numbers []*pair
	for _, line := range lines {
		numbers = append(numbers, parseNumber(line))
	}

	fmt.Println("Problem 1:", Solve1(numbers)) // 4391

	// reset
	numbers = []*pair{}
	for _, line := range lines {
		numbers = append(numbers, parseNumber(line))
	}
	fmt.Println("Problem 2:", Solve2(numbers)) // 4626
}

type pair struct {
	parent   *pair // only nil for master node
	left     *pair // nil if literal
	right    *pair // nil if literal
	leftNum  int   // assume literal if left == nil
	rightNum int   // assume literal if right == nil
}

func Solve1(numbers []*pair) int {
	var curnum *pair
	for i, num := range numbers {
		if i == 0 {
			curnum = num
			continue
		}
		curnum = curnum.Add(num)
		curnum.Reduce()
	}
	return curnum.Magnitude()
}

func Solve2(numbers []*pair) int {
	max := 0
	for i, num1 := range numbers {
		for j, num2 := range numbers {
			if i == j {
				continue // don't do 2x the same numbers
			}
			n1 := parseNumber(num1.String()) // poor man's deepcopy
			n2 := parseNumber(num2.String()) // poor man's deepcopy
			n3 := n1.Add(n2)
			n3.Reduce()
			if max < n3.Magnitude() {
				max = n3.Magnitude()
			}
		}
	}
	return max
}

func parseNumber(line string) *pair {
	r := strings.NewReader(line)
	br := bufio.NewReader(r)
	return newPair(br)
}

func newPair(r *bufio.Reader) *pair {
	p := &pair{}
	// First rune SHOULD always be '['
	ch, _, _ := r.ReadRune()
	if ch != '[' {
		panic(fmt.Sprintf("expected '[', have %s", string(ch)))
	}

	// second rune is either a new pair, or a literal value
	b, _ := r.Peek(1)
	if rune(b[0]) == '[' {
		p.left = newPair(r) // subpair on the left
		p.left.parent = p
	} else {
		// Literal value for left
		ch, _ := r.ReadBytes(',')
		r.UnreadByte() // 1 step back for the ,
		// ch, _, _ := r.ReadRune()
		p.leftNum = aoc.Atoi(string(ch[:len(ch)-1]))
		p.left = nil
	}

	// third rune is always a comma
	ch, _, _ = r.ReadRune()
	if ch != ',' {
		panic(fmt.Sprintf("expected ',', have %s", string(ch)))
	}

	// fourth rune is either a new pair, or a literal value
	b, _ = r.Peek(1)
	if rune(b[0]) == '[' {
		p.right = newPair(r) // subpair on the right
		p.right.parent = p
	} else {
		// Literal value for right
		// ch, _, _ := r.ReadRune()
		ch, _ := r.ReadBytes(']')
		r.UnreadByte() // 1 step back for the ]
		p.rightNum = aoc.Atoi(string(ch[:len(ch)-1]))
		p.right = nil
	}

	// fifth rune is always a closing ']'
	ch, _, _ = r.ReadRune()
	if ch != ']' {
		panic(fmt.Sprintf("expected ']', have %s", string(ch)))
	}

	return p
}

// Magnitude is 3x left and 2x right
func (p *pair) Magnitude() int {
	sum := 0
	if p.left == nil {
		sum += 3 * p.leftNum
	} else {
		sum += 3 * p.left.Magnitude()
	}
	if p.right == nil {
		sum += 2 * p.rightNum
	} else {
		sum += 2 * p.right.Magnitude()
	}
	return sum
}

// Depth from top (explode at 4)
func (p *pair) Depth() int {
	if p.parent == nil {
		return 0
	}
	return p.parent.Depth() + 1
}

// returns true if self or subpair exploded
func (p *pair) Explode() bool {
	// First look for subpairs
	if p.left != nil {
		if p.left.Explode() {
			return true
		}
	}
	if p.right != nil {
		return p.right.Explode()
	}

	// at this point, right value MUST be literal
	// and left value did not explode OR is literal
	// We explode this value if depth is 4 (or larger) and both are literal
	if p.Depth() >= 4 && p.left == nil && p.right == nil {
		// EXPLODE
		p.parent.addleft(p.leftNum, p)
		p.parent.addright(p.rightNum, p)
		p.parent.replace(0, p)
		return true
	}

	return false
}

func (p *pair) replace(v int, sp *pair) {
	if p.left == sp {
		p.left = nil
		p.leftNum = v
	}
	if p.right == sp {
		p.right = nil
		p.rightNum = v
	}
}

func (p *pair) addright(v int, sp *pair) {
	if p.right != sp {
		if p.right == nil { // easy case: left/other pair explodes, right is literal
			p.rightNum += v
			return
		}
		// medium case: left/other explodes, right is pair
		if p.parent == sp {
			p.right.addright(v, p) // from top, don't invert
			return
		}
		p.right.addleft(v, p) // otherwise invert
		return
	}
	// hard case: right explodes, need to go up to find the correct pair
	if p.parent != nil {
		p.parent.addright(v, p)
	}
}

func (p *pair) addleft(v int, sp *pair) {
	if p.left != sp {
		if p.left == nil { // easy case: right/other pair explodes, left is literal
			p.leftNum += v
			return
		}
		// medium case: right/other explodes, left is pair
		if p.parent == sp {
			p.left.addleft(v, p) // from top, don't invert
			return
		}
		p.left.addright(v, p) // otherwise invert
		return
	}
	// hard case: left explodes, need to go up to find the correct pair
	if p.parent != nil {
		p.parent.addleft(v, p)
	}
}

func (p *pair) String() string {
	var l, r string
	if p.left == nil {
		l = fmt.Sprint(p.leftNum)
	} else {
		l = p.left.String()
	}
	if p.right == nil {
		r = fmt.Sprint(p.rightNum)
	} else {
		r = p.right.String()
	}
	return fmt.Sprintf("[%s,%s]", l, r)
}

// recursive split, returns after first split with true, otherwise false
func (p *pair) Split() bool {
	// check left if either literal or pair
	if p.left == nil {
		if p.leftNum > 9 {
			newp := numToPair(p.leftNum)
			newp.parent = p
			p.left = newp
			p.leftNum = 0
			return true
		}
	} else {
		if p.left.Split() {
			return true
		}
	}
	// check right if literal
	if p.right == nil {
		if p.rightNum > 9 {
			newp := numToPair(p.rightNum)
			newp.parent = p
			p.right = newp
			p.rightNum = 0
			return true
		}
		return false
	}
	// right pair
	return p.right.Split()
}

func numToPair(v int) *pair {
	leftnum := v / 2 // integers always round down
	rightnum := leftnum
	if v%2 != 0 { // roundup if there is a remainder
		rightnum++
	}
	return &pair{leftNum: leftnum, rightNum: rightnum}
}

func (p *pair) Add(other *pair) *pair {
	newroot := &pair{left: p, right: other}
	p.parent = newroot
	other.parent = newroot
	return newroot
}

func (p *pair) Reduce() {
	for {
		if !p.Explode() {
			if !p.Split() {
				return
			}
		}
	}
}
