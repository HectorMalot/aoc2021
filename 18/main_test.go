package main

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/hectormalot/aoc2021/aoc"
)

func TestParser(t *testing.T) {
	tests := []struct {
		in  string
		msg string
		out int
	}{
		{in: "[1,2]", msg: "", out: 7},
		{in: "[[1,2],3]", msg: "", out: 27},
		{in: "[[1,2],[[3,4],5]]", out: 143, msg: ""},
		{in: "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]", out: 1384, msg: ""},
		{in: "[[[[1,1],[2,2]],[3,3]],[4,4]]", out: 445, msg: ""},
		{in: "[[[[3,0],[5,3]],[4,4]],[5,5]]", out: 791, msg: ""},
		{in: "[[[[5,0],[7,4]],[5,5]],[6,6]]", out: 1137, msg: ""},
		{in: "[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]", out: 3488, msg: ""},
	}

	for _, test := range tests {
		p := parseNumber(test.in)
		require.Equal(t, test.out, p.Magnitude())
	}
}
func TestDepth(t *testing.T) {
	line := "[[[[[9,8],1],2],3],4]"
	pair := parseNumber(line)
	require.Equal(t, 0, pair.Depth())
	require.Equal(t, 1, pair.left.Depth())
	require.Equal(t, 2, pair.left.left.Depth())
	require.Equal(t, 3, pair.left.left.left.Depth())
	require.Equal(t, 4, pair.left.left.left.left.Depth())
	require.Equal(t, line, pair.String())
}

func TestExplode(t *testing.T) {
	tests := []struct {
		in  string
		out string
		msg string
	}{
		{in: "[1,2]", out: "[1,2]"},
		{in: "[[[[[9,8],1],2],3],4]", out: "[[[[0,9],2],3],4]"},
		{in: "[7,[6,[5,[4,[3,2]]]]]", out: "[7,[6,[5,[7,0]]]]"},
		{in: "[[6,[5,[4,[3,2]]]],1]", out: "[[6,[5,[7,0]]],3]"},
		{in: "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]", out: "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]"},
		{in: "[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]", out: "[[3,[2,[8,0]]],[9,[5,[7,0]]]]"},
	}
	for _, test := range tests {
		pair := parseNumber(test.in)
		pair.Explode()
		require.Equal(t, test.out, pair.String())
	}
}

func TestSplit(t *testing.T) {
	tests := []struct {
		in  string
		out string
		msg string
	}{
		{in: "[1,2]", out: "[1,2]"},
		{in: "[[11,5],4]", out: "[[[5,6],5],4]"},
		{in: "[[10,5],4]", out: "[[[5,5],5],4]"},
		{in: "[[12,5],4]", out: "[[[6,6],5],4]"},
		{in: "[[12,5],[12,5]]", out: "[[[6,6],5],[12,5]]"},
	}
	for _, test := range tests {
		pair := parseNumber(test.in)
		pair.Split()
		require.Equal(t, test.out, pair.String())
	}
}

func TestReduce(t *testing.T) {
	tests := []struct {
		in  string
		out string
		msg string
	}{
		{in: "[1,2]", out: "[1,2]"},
		{in: "[[[[[9,8],1],2],3],4]", out: "[[[[0,9],2],3],4]"},
		{in: "[7,[6,[5,[4,[3,2]]]]]", out: "[7,[6,[5,[7,0]]]]"},
		{in: "[[6,[5,[4,[3,2]]]],1]", out: "[[6,[5,[7,0]]],3]"},
		{in: "[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]", out: "[[3,[2,[8,0]]],[9,[5,[7,0]]]]"},
		{in: "[[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]],[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]]", out: "[[[[7,8],[6,6]],[[6,0],[7,7]]],[[[7,8],[8,8]],[[7,9],[0,6]]]]"},
	}
	for _, test := range tests {
		pair := parseNumber(test.in)
		pair.Reduce()
		require.Equal(t, test.out, pair.String())
	}
}

/*
[[[[4,3],4],4],[7,[[8,4],9]]] + [1,1]:

after addition: [[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]
after explode:  [[[[0,7],4],[7,[[8,4],9]]],[1,1]]
after explode:  [[[[0,7],4],[15,[0,13]]],[1,1]]
after split:    [[[[0,7],4],[[7,8],[0,13]]],[1,1]]
after split:    [[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]
after explode:  [[[[0,7],4],[[7,8],[6,0]]],[8,1]]
*/
func TestE2E1(t *testing.T) {
	num1 := `[[[[4,3],4],4],[7,[[8,4],9]]]`
	num2 := `[1,1]`
	p1 := parseNumber(num1)
	p2 := parseNumber(num2)
	p3 := p1.Add(p2)
	require.Equal(t, "[[[[[4,3],4],4],[7,[[8,4],9]]],[1,1]]", p3.String())
	require.True(t, p3.Explode())
	require.Equal(t, "[[[[0,7],4],[7,[[8,4],9]]],[1,1]]", p3.String())
	require.True(t, p3.Explode())
	require.Equal(t, "[[[[0,7],4],[15,[0,13]]],[1,1]]", p3.String())
	require.False(t, p3.Explode())
	require.Equal(t, "[[[[0,7],4],[15,[0,13]]],[1,1]]", p3.String())
	require.True(t, p3.Split())
	require.Equal(t, "[[[[0,7],4],[[7,8],[0,13]]],[1,1]]", p3.String())
	require.True(t, p3.Split())
	require.Equal(t, "[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]", p3.String())
	require.False(t, p3.Split())
	require.Equal(t, "[[[[0,7],4],[[7,8],[0,[6,7]]]],[1,1]]", p3.String())
	require.True(t, p3.Explode())
	require.Equal(t, "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]", p3.String())

	p3 = p1.Add(p2)
	p3.Reduce()
	require.Equal(t, "[[[[0,7],4],[[7,8],[6,0]]],[8,1]]", p3.String())
}

func TestE2E2(t *testing.T) {
	in := `[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]`
	lines := aoc.Lines(in)
	var numbers []*pair
	for _, line := range lines {
		numbers = append(numbers, parseNumber(line))
	}

	m := Solve1(numbers)
	require.Equal(t, 4140, m)
}

func TestProblem2(t *testing.T) {
	in := `[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]`
	lines := aoc.Lines(in)
	var numbers []*pair
	for _, line := range lines {
		numbers = append(numbers, parseNumber(line))
	}
	m := Solve2(numbers)
	require.Equal(t, 3993, m)
	t.FailNow()
}
