package main

import (
	"fmt"

	"gitlab.com/hectormalot/aoc2021/aoc"
)

func main() {
	fmt.Println(aoc.Lines(aoc.Input(2021, 22)))
}
