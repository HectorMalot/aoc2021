package main

import (
	"fmt"
	"time"

	"github.com/RyanCarrier/dijkstra"
	"gitlab.com/hectormalot/aoc2021/aoc"
)

func main() {
	t1 := time.Now()
	lines := aoc.Lines(aoc.Input(2021, 15))
	cave := parse(lines)
	fmt.Printf("Problem 1: %d (finished in %s)\n", Solve(cave), time.Since(t1))

	t2 := time.Now()
	cave2 := growCave(cave, 5)
	fmt.Printf("Problem 2: %d (finished in %s)\n", Solve(cave2), time.Since(t2))
}

func parse(lines []string) [][]int {
	var cave = make([][]int, len(lines))
	for i, line := range lines {
		cave[i] = aoc.Digits(line)
	}
	return cave
}

// Dijkstra
func Solve(cave [][]int) int {
	maxY, maxX := len(cave)-1, len(cave[0])-1
	graph := dijkstra.NewGraph()

	// vertexes
	for y, row := range cave {
		for x := range row {
			graph.AddMappedVertex(fmt.Sprintf("%d-%d", y, x))
		}
	}

	// arcs
	for y, row := range cave {
		for x, v := range row {
			// top
			if y > 0 {
				graph.AddMappedArc(key(y-1, x), key(y, x), int64(v))
			}
			// bottom
			if y < maxY {
				graph.AddMappedArc(key(y+1, x), key(y, x), int64(v))
			}
			// left
			if x > 0 {
				graph.AddMappedArc(key(y, x-1), key(y, x), int64(v))
			}
			// right
			if x < maxX {
				graph.AddMappedArc(key(y, x+1), key(y, x), int64(v))
			}
		}
	}
	path, _ := graph.Shortest(graph.AddMappedVertex("0-0"), graph.AddMappedVertex(fmt.Sprintf("%d-%d", maxY, maxX)))
	return int(path.Distance)
}

func key(y, x int) string {
	return fmt.Sprintf("%d-%d", y, x)
}

/*
New tiles: all of its risk levels are 1 higher than the tile immediately up or left of it. However, risk levels above 9 wrap back around to 1
*/
func growCave(cave [][]int, n int) [][]int {
	yLen, xLen := len(cave), len(cave[0])
	newCave := make([][]int, yLen*n)
	for y := range newCave {
		newCave[y] = make([]int, xLen*n)
	}

	for i := 0; i < n; i++ { // Y
		for j := 0; j < n; j++ { // X
			for y, row := range cave {
				for x, v := range row {
					newCave[y+yLen*i][x+xLen*j] = ((v + i + j - 1) % 9) + 1
				}
			}
		}
	}

	return newCave
}
